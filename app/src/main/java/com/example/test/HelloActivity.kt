package com.example.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    var showname: TextView? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        showname =findViewById<TextView>(R.id.showname)
        var intent = intent
        showname!!.text = intent.getStringExtra("Name")
        supportActionBar!!.title= "HELLO"


    }

}