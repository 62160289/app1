package com.example.test

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    companion object {
        private const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val Hellobutton: Button = findViewById<Button>(R.id.button2)
        Hellobutton.setOnClickListener {
            val intent = Intent (this,HelloActivity::class.java)
            intent.putExtra("Name","Phankumtong Potisuwan")
            startActivity(intent)
            val nameTextView: TextView = findViewById(R.id.Name)
            val idTextView: TextView = findViewById(R.id.textView2)
            Log.d(TAG,""+nameTextView.text)
            Log.d(TAG,""+idTextView.text)



        }
        supportActionBar!!.title= "HELLO"


    }
}